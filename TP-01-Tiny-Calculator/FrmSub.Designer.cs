﻿namespace TP_01_Tiny_Calculator
{
    partial class FrmSub
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.btnEqual = new System.Windows.Forms.Button();
            this.numOper2 = new System.Windows.Forms.NumericUpDown();
            this.numOper1 = new System.Windows.Forms.NumericUpDown();
            this.lblOperator = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numOper2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOper1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(337, 38);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(312, 12);
            this.txtResult.Name = "txtResult";
            this.txtResult.ReadOnly = true;
            this.txtResult.Size = new System.Drawing.Size(100, 20);
            this.txtResult.TabIndex = 10;
            // 
            // btnEqual
            // 
            this.btnEqual.Location = new System.Drawing.Point(283, 11);
            this.btnEqual.Name = "btnEqual";
            this.btnEqual.Size = new System.Drawing.Size(23, 23);
            this.btnEqual.TabIndex = 9;
            this.btnEqual.Text = "=";
            this.btnEqual.UseVisualStyleBackColor = true;
            this.btnEqual.Click += new System.EventHandler(this.btnEqual_Click);
            // 
            // numOper2
            // 
            this.numOper2.Location = new System.Drawing.Point(157, 12);
            this.numOper2.Name = "numOper2";
            this.numOper2.Size = new System.Drawing.Size(120, 20);
            this.numOper2.TabIndex = 8;
            // 
            // numOper1
            // 
            this.numOper1.Location = new System.Drawing.Point(12, 12);
            this.numOper1.Name = "numOper1";
            this.numOper1.Size = new System.Drawing.Size(120, 20);
            this.numOper1.TabIndex = 6;
            // 
            // lblOperator
            // 
            this.lblOperator.AutoSize = true;
            this.lblOperator.Location = new System.Drawing.Point(138, 14);
            this.lblOperator.Name = "lblOperator";
            this.lblOperator.Size = new System.Drawing.Size(10, 13);
            this.lblOperator.TabIndex = 7;
            this.lblOperator.Tag = "";
            this.lblOperator.Text = "-";
            // 
            // FrmSub
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 73);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.btnEqual);
            this.Controls.Add(this.numOper2);
            this.Controls.Add(this.numOper1);
            this.Controls.Add(this.lblOperator);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSub";
            this.ShowIcon = false;
            this.Text = "Substraction";
            ((System.ComponentModel.ISupportInitialize)(this.numOper2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOper1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Button btnEqual;
        private System.Windows.Forms.NumericUpDown numOper2;
        private System.Windows.Forms.NumericUpDown numOper1;
        private System.Windows.Forms.Label lblOperator;
    }
}