﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_01_Tiny_Calculator
{
    public partial class FrmCalculator : Form
    {
        public FrmCalculator()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMul_Click(object sender, EventArgs e)
        {
            FrmMul frm = new FrmMul();
            frm.ShowDialog();
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {
            FrmDiv frm = new FrmDiv();
            frm.ShowDialog();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FrmAdd frm = new FrmAdd();
            frm.ShowDialog();
        }

        private void btnSub_Click(object sender, EventArgs e)
        {
            FrmSub frm = new FrmSub();
            frm.ShowDialog();
        }
    }
}
